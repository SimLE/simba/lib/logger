/**
 * @file console_logger.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file defina console logger
 * @version 0.1
 * @date 2023-10-20
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef LOCAL_LOGGER_CONSOLE_LOGGER_H_
#define LOCAL_LOGGER_CONSOLE_LOGGER_H_
#include "lib/Ilogger.h"
#include "spdlog/async.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include <spdlog/spdlog.h>

namespace simba {
namespace logger {
namespace local {
class ConsoleLogger : public Ilogger {
 private:
  std::shared_ptr<spdlog::async_logger> logger;

 public:
  /**
   * @brief This funcion initialize logger
   *
   */
  void Init() override;
  /**
   * @brief Adding info log
   *
   * @param log log to save
   */
  void AddInfo(const LOG& log) override;
  /**
   * @brief Adding debug log
   *
   * @param log log to save
   */
  void AddDebug(const LOG& log) override;
  /**
   * @brief Adding error log
   *
   * @param log log to save
   */
  void AddError(const LOG& log) override;

  ConsoleLogger(/* args */);
  ~ConsoleLogger();
};

}  // namespace local
}  // namespace logger
}  // namespace simba
#endif  // LOCAL_LOGGER_CONSOLE_LOGGER_H_
