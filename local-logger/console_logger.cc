/**
 * @file console_logger.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file defina console logger
 * @version 0.1
 * @date 2023-10-20
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "local-logger/console_logger.h"

namespace simba {
namespace logger {
namespace local {

ConsoleLogger::ConsoleLogger(/* args */) { this->Init(); }
ConsoleLogger::~ConsoleLogger() {}

void ConsoleLogger::Init() {
  spdlog::init_thread_pool(8192, 1);
  auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  console_sink->set_pattern("[%d-%m-%Y %H:%M:%S] %v");
  console_sink->set_level(spdlog::level::level_enum::debug);
  std::vector<spdlog::sink_ptr> sinks{console_sink};
  this->logger = std::make_shared<spdlog::async_logger>(
      "testowy", sinks.begin(), sinks.end(), spdlog::thread_pool(),
      spdlog::async_overflow_policy::overrun_oldest);
  this->logger->flush_on(spdlog::level::debug);
  this->logger->set_level(spdlog::level::level_enum::debug);
}

void ConsoleLogger::AddInfo(const LOG& log) {
  this->logger->info(log.GetLog());
}

void ConsoleLogger::AddDebug(const LOG& log) {
  this->logger->debug(log.GetLog());
}

void ConsoleLogger::AddError(const LOG& log) {
  this->logger->error(log.GetLog());
}

}  // namespace local
}  // namespace logger
}  // namespace simba