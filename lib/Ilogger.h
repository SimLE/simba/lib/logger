/**
 * @file Ilogger.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define interface for all loggers
 * @version 0.1
 * @date 2023-10-20
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef LIB_ILOGGER_H_
#define LIB_ILOGGER_H_
#include "lib/log.h"
namespace simba {
namespace logger {
class Ilogger {
 private:
  /* data */
 public:
  virtual void Init() = 0;
  virtual void AddInfo(const LOG& log) = 0;
  virtual void AddDebug(const LOG& log) = 0;
  virtual void AddError(const LOG& log) = 0;
  virtual ~Ilogger() = default;
};
}  // namespace logger
}  // namespace simba
#endif  // LIB_ILOGGER_H_
