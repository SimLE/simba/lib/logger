/**
 * @file log.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define log objects
 * @version 0.1
 * @date 2023-10-20
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "lib/log.h"

namespace simba {
namespace logger {
LOG::LOG(const std::string& function_name, const std::string text)
    : function_{function_name}, text_{text} {}

LOG::LOG(const std::string text) : function_{""}, text_{text} {}

const std::string LOG::GetLog() const {
  if (function_.length() > 0)
    return std::string{"[" + function_ + "] " + text_};
  else
    return std::string{text_};
}
}  // namespace logger
}  // namespace simba
