/**
 * @file log.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define log objects
 * @version 0.1
 * @date 2023-10-20
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef LIB_LOG_H_
#define LIB_LOG_H_
#include <string>

namespace simba {
namespace logger {
class LOG {
 protected:
  const std::string function_;
  const std::string text_;

 public:
  LOG(const std::string& function_name, const std::string text);
  LOG(const std::string text);
  const std::string GetLog() const;
};
}  // namespace logger
}  // namespace simba
#endif  // LIB_LOG_H_
