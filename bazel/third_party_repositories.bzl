load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

all_content = """filegroup(name = "all", srcs = glob(["**"]), visibility = ["//visibility:public"])"""

def include_third_party_repositories():

    http_archive(
        name = "com_github_fmtlib_fmt",
        # sha256 = "4c0741e10183f75d7d6f730b8708a99b329b2f942dad5a9da3385ab92bb4a15c",
        strip_prefix = "fmt-10.1.1",
        urls = ["https://github.com/fmtlib/fmt/archive/refs/tags/10.1.1.zip"],
        build_file = "//bazel:fmtlib.BUILD",
    )

    http_archive(
        name = "com_github_gabime_spdlog",
        build_file = "//bazel:spdlog.BUILD",
        # sha256 = "3cc41508fcd79e5141a6ef350506ef82982ca42a875e0588c02c19350ac3702e",
        strip_prefix = "spdlog-1.12.0",
        urls = ["https://github.com/gabime/spdlog/archive/refs/tags/v1.12.0.zip"],
    )